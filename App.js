import React from 'react';
import { Grid, Paper, Typography } from '@mui/material';

const SquareBox = ({ children }) => {
  return (
    <div style={{ position: 'relative', width: '100%', paddingTop: '100%' }}>
      <div style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }}>{children}</div>
    </div>
  );
};

const MyGrid = () => {
  return (
    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Paper style={{ padding: '75px', border: '1px solid #000', backgroundColor: 'lightblue', height: '400px', width: '50%', margin: 'auto' }}>
            <Typography variant="h5">House</Typography>
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <SquareBox>
                  <Paper style={{ backgroundColor: '#00ff0080', height: '30%', width: '30%', border: '1px solid #000', textAlign:'left' }}>
                    Window1
                  </Paper>
                </SquareBox>
              </Grid>
              <Grid item xs={6} container direction="column" justifyContent="right">
                <Grid item>
                  <SquareBox>
                    <Paper style={{ backgroundColor: '#00ff0080', height: '30%', width: '30%', border: '1px solid #000', textAlign:'center' }}>
                      Window2
                    </Paper>
                  </SquareBox>
                </Grid>
                <Grid item style={{ display: 'flex', justifyContent: 'end' }}>
                  <Paper style={{ backgroundColor: '#00ff0080', height: '439px', width: '180px', border: '1px solid #000', textAlign:'center', position: 'relative' }}>
                    <div style={{ position: 'absolute', top: '50%', transform: 'translateY(-50%)' }}>
                      Door
                    </div>
                  </Paper>
                </Grid>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

export default MyGrid;
